# coding=utf-8

import re
from decimal import Decimal

from django import forms
from django.db import transaction

from money.models import NewUser


class MoneyTransferForm(forms.Form):
    user = forms.ModelChoiceField(
        queryset=NewUser.objects.all(),
        widget=forms.Select(),
        label=u'Выберите пользователя для списания средств:'
    )
    inn = forms.CharField(
        widget=forms.TextInput(),
        label=u'Ввведите ИНН на которые будут переведены средства:'
    )
    bill = forms.DecimalField(
        max_digits=10,
        decimal_places=2,
        label='Введите сумму для перевода:')

    def clean_inn(self):
        data = super(MoneyTransferForm, self).clean()
        recipients = re.findall(r'\w+', data.get('inn'))
        available_inn = NewUser.objects.filter(inn__in=recipients).values_list('inn', flat=True)
        not_available_inns = set(recipients).difference(available_inn)
        if not_available_inns:
            raise forms.ValidationError (u'ИНН %s Нет в списке доступных' % ', '.join(not_available_inns))
        return self.cleaned_data['inn']

    def clean_bill(self):
        data = super(MoneyTransferForm, self).clean()
        sender = data.get('user')
        available_money = sender.bill
        transfer_money = data.get('bill')
        if available_money < transfer_money:
            raise forms.ValidationError(u'У %s недостаточно средств' % sender.username)
        if transfer_money <= 0:
            raise forms.ValidationError(u'Сумма для перевода должна быть больше 0')
        return self.cleaned_data['bill']

    @transaction.atomic
    def _make_operations(self):
        data = super(MoneyTransferForm, self).clean()
        transfer_money = data.get('bill')
        recipients = re.findall(r'\d+', data.get('inn'))
        users = NewUser.objects.filter(inn__in=recipients)
        part_to_send = round(transfer_money / len(recipients), 2)
        for user in users:
            user.bill += Decimal(part_to_send)
            user.save()
        sender = data.get('user')
        sender.bill -= transfer_money
        sender.save()
