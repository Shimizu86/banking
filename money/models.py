from django.contrib.auth.models import User, UserManager
from django.db import models


class NewUser(User):
    inn = models.CharField(unique=False, max_length=12)
    bill = models.DecimalField(max_digits=9, decimal_places=2)

    objects = UserManager()
