from django.conf.urls import url

from money.views import TransferView, MoneyBalanceView

urlpatterns = [
    url(r'^transfer/', TransferView.as_view(template_name='money_transfer.html'), name='transfer'),
    url(r'^remains/', MoneyBalanceView.as_view(template_name='money_balance.html'), name='money_remain'),
]
