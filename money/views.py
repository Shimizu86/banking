from django.http import JsonResponse
from django.views.generic import FormView, ListView

from money.forms import MoneyTransferForm
from money.models import NewUser


class TransferView(FormView):
    model = NewUser
    form_class = MoneyTransferForm

    def form_valid(self, form):
        form._make_operations()
        return JsonResponse({'status': 'ok'})

    def form_invalid(self, form):
        return JsonResponse({'status': 'error', 'errors': form.errors})


class MoneyBalanceView(ListView):
    model = NewUser
    context_object_name = 'user_list'
