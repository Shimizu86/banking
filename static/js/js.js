$(document).ready(function(){
    var content = $('#form'),
        success_text = $("#success_text"),
        error_text = $("#error_text");
    success_text.hide();
    error_text.hide();
    content.submit(function(e){
        e.preventDefault();
        $.ajax({
            url: $('#form'),
            type: 'post',
            data: content.serialize(),
            success : function(data){
                $(".form-group").addClass("has-success");
                success_text.hide();
                error_text.hide();
                if (data.status == 'error'){
                    for (error in data.errors){
                        error_text.show().html(data.errors[error]);
                    }
                }
                if (data.status == 'ok'){
                    success_text.show();
                }
            }
        })
    });
});